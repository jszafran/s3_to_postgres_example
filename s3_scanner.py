import boto3

class s3scanner:
    def __init__(self, bucket):
        """
        Initialize s3 resource with bucket name provided
        """
        self.s3 = boto3.resource('s3')
        self.bucket = self.s3.Bucket(bucket)

    def list_files(self, scan_path):
        """
        Function will return list of files with matching prefix.
        If prefix is not provided, all files from bucket will be listed.
        """
        if scan_path == None:
            scan_path = ''
        if len(scan_path) > 0 and scan_path[-1] != '/':
            scan_path = scan_path + '/'
        res = [ x.key.replace(scan_path, '') for x in self.bucket.objects.filter(Prefix=scan_path) ]
        return list(filter(lambda x: len(x) > 0, res))


scanner = s3scanner('inbound-manage-users')

print(scanner.list_files(None))