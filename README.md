# s3_to_postgres_example

This is proof of concept for creating an automated data pipeline between AWS S3 storage and PostgreSQL database (hosted through Amazon RDS). 
We will also use EC2 instance for hosting python script that will deal with all incoming files.